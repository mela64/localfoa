# Local Laws of Focus of Attention

Python implementation of the Local Laws of Focus of Attention (FOA) that completely redesign the idea of estimating the trajection of the FOA by means of local update procedures, following wave equations or diffusion equations. The implemenation is based on OpenCV to handle the input data (video, images, webcam, ...), while PyTorch is used to handle the features associated to the gravitational masses. Then, routines from the SciPy are used to integrate the ODE and/or to update the potential term. 

#### How to use it?
	Requirements:
        pip install matplotlib numpy opencv-python scipy torch
        
	Usage:
        python run.py
        
 See the **run.py** file to edit options, customize the FOA trajectory and define the appropriate solving scheme.
 
 #### File description:
 * **run.py**: the main script thats load the input data, runs the FOA computation, draws the FOA on screen.
 * **geymolloc.py**: the class with the __GEymolLoc__ object that contains our implementation of the local laws.
 * **geymolloc_utils.py**: several utility functions used to prepare the input data.
 * **/data**: folder containing some test images and videos.  

        

