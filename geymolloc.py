"""Python implementation of a local version of the gravitational laws of Focus of Attention (2020)"""
import time
import os
import shutil
from math import isnan, sqrt
from random import randint, uniform
from scipy.integrate import odeint
from scipy.interpolate import RectBivariateSpline
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy.sparse
import scipy.sparse.linalg
import torch
import numpy as np
import cv2


class GEymolLoc:
    """This is the main class of the Focus of Attention (FOA) module.

    This object expects a number of options that are contained in the "parameters" argument (dictionary).
    The parameters are listed in what follows, and missing non-mandatory options are set to their default values.

    WARNING: Despite the support of GPU devices, this code does not strongly benefit from GPU, since data is passed
    back and forth between CPU and GPU. The reason why we kept the GPU support is because in future implementations
    we plan to get rid of the current ODE integrator, allowing the algorithm to fully run on GPU.

    w (int):                width of the expected input frame (>0).
    h (int):                height of the expected input frame (>0).
    fps (float):            frames-per-seconds of the input stream (def: 30.0).
    y (list of 4 floats):   FOA initial position [row, column, row velocity, columns velocity] (def: approx. center).

    alpha_c (float):        weight of the brightness-related mass (>=0).
    alpha_of (float):       weight of the motion-related mass (>=0).
    alpha_fm (float)        weight of the detected-faces-related mass (>=0).
    alpha_virtual (float):  weight of a temporarily activated mass (>=0).
    alpha_scale (float):    a global scale term for the whole gravitational mass (def. 1.0).
    force_scale (float):    scale of the force term (>0).
    dissipation (float):    dissipation term in the FOA equation (>=0).
    fix_threshold (float):  norm of the velocity under which we have fixations (>=0) (def. 5% of max(w,h)).

    method (str):           the method to update the potential, 'wave' or 'diffusion'.
    c (float):              speed propagation (if method is 'wave') or diffusion coefficient (if method is 'diffusion').
    I (function):           function to initialize the potential, I(x,y), for both 'wave' and 'diffusion' (def: None).
    epsilon (float):        parameter of the finite difference method to evaluate the gradient of the potential.
    w_absorbing (float):    damping term in the case in which method is 'wave'.
    w_abs_layer (integer):    width of an additional absorbing layer to suppress spurious boundary reflections in the 'wave' case.
                            It has to be '0' in the 'diffusion' case.
    w_gamma (float):        multiplicative factor of the second order time derivative if method is 'wave'.
    w_eval (str):           evaluation procedure, 'implicit', 'explicit_1' or 'explicit_2' (if method is 'wave') (def: 'implicit').
                            In the 'explicit_1' case, w_gamma can not be zero.
                            In this case, the finite difference approximation of the first order temporal derivative is a central one.
                            In the 'explicit_2' case the approximation is the backward one instead.
    w_V (function):         function to initialize the velocity in potential, V(x,y), if method is 'wave' (def: None).
    d_theta (float):        scheme for the update of the potential in [0,1] (if method is 'diffusion').

    plot (bool):            turn debug plot on/off (def: False).
    plot_method (int):      if debug 'plot' is True then this parameter can be 1 (simple), 2 (3d plot), 3 (heat map) (def: 1).
    save_plot (bool):       save debug plot(s) (def: False).

    Args:
        parameters (dict): dictionary of options to customize the FOA module.
        device (str): PyTorch identifier of the computing device (ODE integration is always done in "cpu")
    """

    def __init__(self, parameters, device="cpu"):

        # ensuring that required parameters are provided and they have valid values
        assert 'w' in parameters and parameters['w'] > 0, \
            "Missing w parameter or invalid value! (it must be >0)"
        assert 'h' in parameters and parameters['h'] > 0, \
            "Missing h parameter or invalid value! (it must be >0)"
        assert 'alpha_c' in parameters and parameters['alpha_c'] >= 0, \
            "Missing alpha_c parameter or invalid value! (it must be >=0)"
        assert 'alpha_of' in parameters and parameters['alpha_of'] >= 0, \
            "Missing alpha_of parameter or invalid value! (it must be >=0)"
        assert 'alpha_fm' in parameters and parameters['alpha_fm'] >= 0, \
            "Missing alpha_fm parameter or invalid value! (it must be >=0)"
        assert 'alpha_virtual' in parameters and parameters['alpha_virtual'] >= 0, \
            "Missing alpha_virtual parameter or invalid value! (it must be >=0)"
        assert 'dissipation' in parameters and parameters['dissipation'] >= 0, \
            "Missing dissipation parameter or invalid value! (it must be >=0)"
        assert 'method' in parameters and (parameters['method'] == 'wave' or parameters['method'] == 'diffusion'), \
            "Missing method parameter or invalid value! (it must be 'wave' or 'diffusion')"
        assert 'c' in parameters and parameters['c'] > 0, \
            "Missing c parameter or invalid value! (it must be >0)"
        assert 'w_absorbing' in parameters and parameters['w_absorbing'] >= 0, \
            "Missing w_absorbing parameter or invalid value! (it must be >=0)"
        assert 'w_abs_layer' in parameters and parameters['w_abs_layer'] >= 0, \
            "Missing w_abs_layer parameter or invalid value! (it must be >=0)"
        assert 'w_gamma' in parameters and parameters['w_gamma'] >= 0, \
            "Missing w_gamma parameter or invalid value! (it must be >=0)"
        assert 'd_theta' in parameters and 0 <= parameters['d_theta'] <= 1, \
            "Missing d_theta parameter or invalid value! (it must be in [0,1])"
        assert 'force_scale' in parameters and parameters['force_scale'] > 0, \
            "Missing force_scale parameter or invalid value! (it must be >0)"
        assert 'epsilon' in parameters and parameters['epsilon'] >= 0, \
            "Missing epsilon parameter or invalid value! (it must be >0)"
        assert 'fps' in parameters and parameters['fps'] > 0, \
            "Missing fps parameter or invalid value! (it must be >0)"

        # shortcuts to the provided static parameters (and setting default values of some missing parameters)
        self.p = parameters
        self.parameters = parameters  # kept for backward compatibility
        self.w = parameters['w']
        self.h = parameters['h']
        self.w_abs_layer = parameters['w_abs_layer']
        self.w_tilde = self.w + 2 * self.w_abs_layer
        self.h_tilde = self.h + 2 * self.w_abs_layer
        self.fps = parameters['fps']
        self.delta_t = 1.0 / self.fps
        if 'y' in parameters and parameters['y'] is not None:
            self.y = np.array(parameters['y']).astype(np.float64)
        else:
            self.y = GEymolLoc.__generate_initial_conditions(self.h, self.w)
            self.y[0] = self.y[0] + self.w_abs_layer
            self.y[1] = self.y[1] + self.w_abs_layer
        self.device = torch.device("cpu") if device is None else torch.device(device)
        self.method = parameters['method']
        self.c = parameters['c']
        self.I = parameters['I'] if 'I' in parameters else None
        self.w_V = parameters['w_V'] if 'w_V' in parameters else None
        self.w_eval = parameters['w_eval'] if 'w_eval' in parameters else 'implicit'
        self.plot = parameters['plot'] if 'plot' in parameters else False
        self.plot_method = parameters['plot_method'] if 'plot_method' in parameters else 1
        self.save_plot = parameters['save_plot'] if 'save_plot' in parameters else False  # True to save plots
        self.pytorch_friendly = self.method == 'wave' and (self.w_eval == 'explicit_1' or self.w_eval == 'explicit_2')

        # misc quantities
        self.t = 0  # time index
        self.first_call_to_potential_update = True  # flag to indicate the first call of an update potential method
        self.s_kernel = GEymolLoc.__get_sobel_kernel(self.device)  # kernel of the Sobel operator
        self.IOR_matrix = torch.zeros((self.h, self.w), dtype=torch.float32, device=self.device)  # IOR matrix
        self.X1torch, self.X2torch = torch.meshgrid(torch.arange(0, self.h), torch.arange(0, self.w))  # 2d coordinates
        self.X1torch = self.X1torch.to(torch.float32).to(self.device)  # cast to float
        self.X2torch = self.X2torch.to(torch.float32).to(self.device)  # cast to float

        # face detector and face map
        self.face_detector = cv2.CascadeClassifier(os.path.dirname(cv2.__file__) + os.sep + "data" +
                                                   os.sep + 'haarcascade_frontalface_default.xml')
        self.face_map = torch.zeros((self.h, self.w), dtype=torch.float32, device=self.device)

        # fields that will be initialized by the following call to self.__initialize_potential()
        self.source = None  # (minus) mu, the mass term at time t
        self.source_1 = None  # (minus) mu at time t-1
        self.frame_1 = None  # frame at time t-1 (ref. only)
        self.gradient_norm_1 = None  # gradient norm at time t-1 (ref. only)
        self.Cx2 = None  # scalar coefficient needed when updating the potential
        self.Cy2 = None  # scalar coefficient needed when updating the potential
        self.Cx = None  # scalar coefficient needed when updating the potential
        self.Cy = None  # scalar coefficient needed when updating the potential
        self.u = None  # potential term at time t
        self.u_1 = None  # potential term at time t-1
        self.u_2 = None  # potential term at time t-2
        self.x1 = None  # integer coordinates in [0,h]
        self.x2 = None  # integer coordinates in [0,w]
        self.range_x1 = None  # range(0,h)
        self.range_x2 = None  # range(0,w)
        self.v = None  # velocity-related potential
        self.m = None  # lambda function to convert 2d coordinates to a single integer index
        self.b = None  # vector b of the sparse system Ax=b that is solved to update the potential
        self.A = None  # matrix A of the sparse system Ax=b that is solved to update the potential
        self.A_first_update = None  # matrix A (alt) of the sparse system Ax=b that is solved to update the potential
        self.X1 = None  # plot mesh (first coordinate)
        self.X2 = None  # plot mesh (second coordinate)

        # checking values and initializing the gravitational potential
        self.__check_stability()
        self.__initialize_potential()

        # moving data to GPU (or other devices), if needed
        if self.pytorch_friendly:
            self.u = torch.from_numpy(self.u).to(torch.float).to(self.device) if self.u is not None else None
            self.u_1 = torch.from_numpy(self.u_1).to(torch.float).to(self.device) if self.u_1 is not None else None
            self.u_2 = torch.from_numpy(self.u_2).to(torch.float).to(self.device) if self.u_2 is not None else None
            self.v = torch.from_numpy(self.v).to(torch.float).to(self.device) if self.v is not None else 0.0
            self.wave_dissipation_function_a = \
                torch.from_numpy(self.wave_dissipation_function_a).to(torch.float).to(self.device) \
                    if self.wave_dissipation_function_a is not None else 0.0

        if self.plot:
            self.__initialize_plot_data()

    def time_step(self, frame, of, lock=None, frame_gray_uint8_cpu=None, virtualmass_xy=None, virtualmass_vxvy=None):
        """This is the main method, it computes the FOA coordinates and speed at the current time instant.

        Args:
            frame (PyTorch tensor): grayscale input frame/image at the current time, pixels in [0,1], 1 x 1 x h x w.
            of (PyTorch tensor): the optical flow tensor (actually the pixel displacements, float),  1 x 2 x h x w.
            lock: when need, this is a thread lock, to avoid concurrent executions of the ODE solver (SciPy issue).
            frame_gray_uint8_cpu (NumPy tensor): grayscale input frame/image, pixels in [0,255], h x w x 1
            virtualmass_xy (list of 2 floats): coordinates of a temporary unary mass.
            virtualmass_vxvy (list of 2 floats): speed of a temporary unary mass, transferred to the initial FOA speed.

        Returns:
            An array (NumPy array) with 4 floats, that are the FOA coordinates (row, column) and speed (rows, columns)
        """

        # ensuring data is well shaped
        if frame.ndim != 4 or frame.shape[0] != 1 or frame.shape[1] != 1:
            raise ValueError("Unsupported tensor format for the input frame: " + str(frame.shape) +
                             " (expected 1 x 1 x h x w)")
        if of is not None and (of.ndim != 4 or of.shape[0] != 1 or of.shape[1] != 2):
            raise ValueError("Unsupported tensor format for the optical flow data: " + str(of.shape) +
                             " (expected 1 x 2 x h x w)")

        if frame.shape[2] != self.h or frame.shape[3] != self.w:
            raise ValueError("Invalid input width and/or height (expected " + str(self.w) + "x" + str(self.h)
                             + ", got " + str(frame.shape[3]) + "x" + str(frame.shape[2]) + ")")

        # shortcuts (we assume that some options can be updated while the algorithm is running by an external tool)
        alpha_scale = self.p['alpha_scale'] if 'alpha_scale' in self.p else 1.0
        alpha_c = self.p['alpha_c']
        alpha_of = self.p['alpha_of']
        alpha_fm = self.p['alpha_fm']
        alpha_virtual = self.p['alpha_virtual']
        fix_threshold = self.p['fix_threshold'] if 'fix_threshold' in self.p else max(self.w, self.h) * 0.05
        fix_threshold = self.p['fixation_threshold_speed'] if 'fixation_threshold_speed' in self.p else fix_threshold
        u = None

        # computing values of the considered masses over the frame coordinates
        if self.frame_1 is None or self.frame_1.storage().data_ptr() != frame.storage().data_ptr():
            gradient_norm_t = GEymolLoc.__get_gradient_norm(frame, self.s_kernel) * (1.0 - self.IOR_matrix) \
                if alpha_c > 0.0 else torch.tensor(0.0, device=self.device)
            self.frame_1 = frame
            self.gradient_norm_1 = gradient_norm_t
        else:
            gradient_norm_t = self.gradient_norm_1 * (1.0 - self.IOR_matrix) \
                if alpha_c > 0.0 else torch.tensor(0.0, device=self.device)
        of_norm_t = GEymolLoc.__get_opticalflow_norm(of) \
            if of is not None and alpha_of > 0.0 else torch.tensor(0.0, device=self.device)
        face_map_t = self.face_map * (1.0 - self.IOR_matrix) \
            if alpha_fm > 0.0 else torch.tensor(0.0, device=self.device)
        virtualmass_norm_t = GEymolLoc.__get_virtualmass_norm(frame, virtualmass_xy) \
            if virtualmass_xy is not None and alpha_virtual > 0.0 else torch.tensor(0.0, device=self.device)

        of_norm_t = GEymolLoc.__normalize(of_norm_t)

        # transferring the virtual mass speed to the starting FOA coordinates (conventionally defined as it is)
        if virtualmass_xy is not None and virtualmass_vxvy is not None and alpha_virtual > 0.0:
            self.y[2] = virtualmass_vxvy[0]
            self.y[3] = virtualmass_vxvy[1]

        # building the scaled (minus) mass term over the frame coordinates
        self.source = (-alpha_scale * (alpha_c * gradient_norm_t + alpha_of * of_norm_t
                                       + alpha_fm * face_map_t + alpha_virtual * virtualmass_norm_t))
        if not self.pytorch_friendly:
            self.source = self.source.cpu().numpy()
            self.source = np.pad(self.source, ((self.w_abs_layer, self.w_abs_layer),
                                               (self.w_abs_layer, self.w_abs_layer)))
        else:
            self.source = torch.nn.functional.pad(self.source, [self.w_abs_layer, self.w_abs_layer,
                                                                self.w_abs_layer, self.w_abs_layer])

        # case 1/2: we are at t=0, the potential "u" is assumed to be zero everywhere,
        # while "u_1" has been previously initialized using function I(x,y), and that is what we handle here
        if self.t == 0:
            u = self.u_1

        # case 2/2: we are at t>0, the potential "u" must be updated first
        # then we can continue as in case 1, focussing on "u" and no more on "u_1"
        elif self.t > 0:
            if self.method == 'wave' and self.w_eval == 'explicit_1':
                self.__update_potential_wave_explicit_1(call1=self.first_call_to_potential_update)
            if self.method == 'wave' and self.w_eval == 'explicit_2':
                self.__update_potential_wave_explicit_2(call1=self.first_call_to_potential_update)
            elif self.method == 'wave' and self.w_eval == 'implicit':
                self.__update_potential_wave_implicit(call1=self.first_call_to_potential_update)
            elif self.method == 'diffusion':
                self.__update_potential_diffusion()
            self.first_call_to_potential_update = False

            u = self.u  # do not move this before the calls to __update_potential_whatever!

        # both case 1/2 and case 2/2: preparing interpolator and plotting stuff
        u_interpolator = RectBivariateSpline(self.x1, self.x2, u, kx=3, ky=3, s=0) if not self.pytorch_friendly else u

        if self.plot:
            GEymolLoc.__plot(u, u_interpolator, self.X1, self.X2, self.t, self.y[0], self.y[1],
                             frame, self.fig, self.ax, self.ax_figure, self.cax, self.w_abs_layer,
                             self.h, self.w, counter=self.plot_counter, dpi=self.dpi,
                             vmin1=self.vmin1, vmax1=self.vmax1, vmin2=self.vmin2, vmax2=self.vmax2,
                             plot_method=self.plot_method, save_plot=self.save_plot,
                             ax_details=self.ax_details, gradient_norm_t=-alpha_scale * alpha_c * gradient_norm_t,
                             ax_motion=self.ax_motion, of_norm_t=-alpha_scale * alpha_of * of_norm_t)
            self.plot_counter += 1

        # both case 1/2 and case 2/2: we update the FOA coordinates
        # The evaluation of the gravitational force is in __update_position_and_velocity
        y_prev = self.y

        start_time = time.time()
        self.y = self.__update_position_and_velocity(u_interpolator, lock=lock)
        ode_time = time.time() - start_time

        # case 2/2 only: updating references (mostly variable swaps for the next call to this method)
        if self.t > 0:
            if self.method == 'wave':
                self.u_2 = self.u_1
                self.u_1 = self.u
                self.u = self.u_2  # it looks weird, it is just to set "u" to a valid memory area (for wave-explicit)
            elif self.method == 'diffusion':
                self.u_1, self.u = self.u, self.u_1  # u_1' = u; u' = u_1 (in other words, swapping "u" and "u_1"
                self.source_1 = self.source

        # next time instant
        self.t += self.delta_t

        # avoid predicting out-of-frame locations
        GEymolLoc.__stay_inside_fix_nans(self.y, (self.h, self.w), self.w_abs_layer)

        # detecting saccadic movements
        vel_norm = sqrt(((float(self.y[0]) - y_prev[0]) / self.delta_t) ** 2 +
                        ((float(self.y[1]) - y_prev[1]) / self.delta_t) ** 2)
        saccade = vel_norm > fix_threshold

        # updating IOR matrix
        self.IOR_matrix = self.__update_inhibition_of_return(self.IOR_matrix, row_col=[self.y[0] - self.w_abs_layer,
                                                                                       self.y[1] - self.w_abs_layer])

        # updating the face map
        if alpha_fm > 0.0:
            self.__update_face_map(frame, frame_gray_uint8_cpu=frame_gray_uint8_cpu)

        return self.y, saccade, ode_time

    def reset(self, y=[], t=0):
        """Reset the FOA coordinates and the time index."""

        self.t = t

        if self.y is None:
            self.y = GEymolLoc.__generate_initial_conditions(self.h, self.w)
            self.y[0] = self.y[0] + self.w_abs_layer
            self.y[1] = self.y[1] + self.w_abs_layer
        else:
            self.y = np.array(y)

    # defining an alias of the "time_step" method, for backward compatibility
    next_location = time_step

    def __wave_dissipation_function(self, x, y):
        w_abs_layer = self.p['w_abs_layer']
        w_absorbing = self.p['w_absorbing']
        w = self.w
        h = self.h

        if 'w_abs_case' not in self.p:
            return w_absorbing
        elif self.p['w_abs_case'] == 'function':
            if 0 <= x < w_abs_layer and 0 <= y < w_abs_layer:
                return (w_abs_layer - x) ** 2 + (w_abs_layer - y) ** 2 + w_absorbing
            elif 0 <= x < w_abs_layer <= y < h + w_abs_layer:
                return (w_abs_layer - x) ** 2 + w_absorbing
            elif 0 <= x < w_abs_layer and h + w_abs_layer <= y < h + 2 * w_abs_layer:
                return (w_abs_layer - x) ** 2 + (y - w_abs_layer - h + 1) ** 2 + w_absorbing
            elif w_abs_layer <= x < w + w_abs_layer and h + w_abs_layer <= y < h + 2 * w_abs_layer:
                return (y - w_abs_layer - h + 1) ** 2 + w_absorbing
            elif w + w_abs_layer <= x < w + 2 * w_abs_layer and h + w_abs_layer <= y < h + 2 * w_abs_layer:
                return (x - w_abs_layer - w + 1) ** 2 + (y - w_abs_layer - h + 1) ** 2 + w_absorbing
            elif w + w_abs_layer <= x < w + 2 * w_abs_layer and w_abs_layer <= y < h + w_abs_layer:
                return (x - w_abs_layer - w + 1) ** 2 + w_absorbing
            elif w + w_abs_layer <= x < w + 2 * w_abs_layer and 0 <= y < w_abs_layer:
                return (x - w_abs_layer - w + 1) ** 2 + (w_abs_layer - y) ** 2 + w_absorbing
            elif w + w_abs_layer > x >= w_abs_layer > y >= 0:
                return (w_abs_layer - y) ** 2 + w_absorbing
            else:
                return w_absorbing
        elif self.p['w_abs_case'] == 'constant':
            return w_absorbing

    @staticmethod
    def __generate_initial_conditions(h, w):
        """Generate the initial position of the FOA"""

        init_ray = int(min(h, w) * 0.17)  # arbitrary (it could be improved)
        x1_init = int(h / 2) + randint(-init_ray, init_ray)  # arbitrary (it could be improved)
        x2_init = int(w / 2) + randint(-init_ray, init_ray)  # arbitrary (it could be improved)
        v1_init = 2.0 * uniform(0.3, 0.7) * ((-1) ** randint(0, 1))  # arbitrary (it could be improved)
        v2_init = 2.0 * uniform(0.3, 0.7) * ((-1) ** randint(0, 1))  # arbitrary (it could be improved)

        return np.array([x1_init, x2_init, v1_init, v2_init]).astype(np.float64)

    # OLD VERSION
    # def __check_stability(self):
    #     """Ensure that the selected parameters do not lead to solutions that will progressively degenerate"""
    #
    #     stability_limit = 0
    #     w_gamma = self.parameters['w_gamma']
    #     w_absorbing = self.parameters['w_absorbing']
    #
    #     if self.method == 'wave':
    #         if self.w_eval == 'explicit_1':
    #             stability_limit = sqrt(w_gamma) / (sqrt(2) * self.delta_t)
    #         elif self.w_eval == 'explicit_2':
    #             stability_limit = sqrt((w_gamma /(2 * self.delta_t**2)) + (w_absorbing /(4*self.delta_t)))
    #         elif self.w_eval == 'implicit':
    #             stability_limit = -1
    #     if self.method == 'diffusion':
    #         if self.p['d_theta'] < 0.5:
    #             stability_limit = 1 / (4 * self.delta_t)
    #         else:
    #             stability_limit = -1  # when 0.5 < d_theta <= 1, the algorithm is numerically stable for any c
    #
    #     if 0 < stability_limit < self.c:
    #         raise ValueError('Stability check failed, c=%g exceeds the stability limit %g' % (self.c, stability_limit))
    #         #  raise ValueError('Stability check failed, c=%g exceeds the stability limit %g' % (self.c, stability_limit))

    # NEWEST VERSION in terms of fps
    def __check_stability(self):
        """Ensure that the selected parameters do not lead to solutions that will progressively degenerate"""

        stability_limit = 0
        w_gamma = self.parameters['w_gamma']
        w_absorbing = self.parameters['w_absorbing']

        if self.method == 'wave':
            if self.w_eval == 'explicit_1':
                stability_limit = sqrt(2) * self.c / sqrt(w_gamma)
            elif self.w_eval == 'explicit_2':
                stability_limit = 4 * self.c ** 2 / (
                            (w_absorbing / 2) + sqrt((w_absorbing ** 2 / 4) + 8 * w_gamma * self.c ** 2))
            elif self.w_eval == 'implicit':
                stability_limit = 0

        if self.method == 'diffusion':
            if self.p['d_theta'] < 0.5:
                stability_limit = 4 * self.c
            else:
                stability_limit = 0  # when 0.5 < d_theta <= 1, the algorithm is numerically stable for any c

        if self.fps < stability_limit:
            raise ValueError(
                'Stability check failed, fps=%g is below the stability limit %g' % (self.fps, stability_limit))

    def __initialize_potential(self):
        """Initialize data needed to handle the gravitational potential, mostly in wave+implicit and diffusion."""

        # initializing the field that will host the mass term at time t-1
        if self.method == 'diffusion':
            self.source_1 = np.zeros((self.h, self.w))  # (minus) mass at time t-1, shape=(h,w)

        # assuming pixel-grid space of 1px, 2D analogous to the Courant number
        if self.method == 'wave':
            self.Cx2 = (self.c * self.delta_t) ** 2
            self.Cy2 = self.Cx2
            self.conv2d_derivative = torch.tensor([[[[0.0, self.Cy2, 0.0],
                                                     [self.Cx2, -2.0 * (self.Cx2 + self.Cy2), self.Cx2],
                                                     [0.0, self.Cy2, 0.0]]]], dtype=torch.float, device=self.device)

        #  assuming pixel-grid space of 1px, Fourier diffusion coefficient
        elif self.method == 'diffusion':
            self.Cx = self.c * self.delta_t
            self.Cy = self.Cx

        # creating data for the gravitational potential array
        self.u = np.zeros((self.h_tilde, self.w_tilde))  # potential at time t, shape=(h,w)
        self.u_1 = None  # solution at time t-1, shape=(h,w), for convenience it will be initialized below using I(x,y)
        self.u_2 = None  # solution at time t-2, shape=(h,w)
        if self.method == 'wave':
            self.u_2 = np.zeros((self.h_tilde, self.w_tilde))

        # creating coordinates used when interpolating, plotting, and computing the IOR gaussian
        self.x1 = np.arange(0, self.h_tilde)  # [0,1,2,...,h-1] (h)
        self.x2 = np.arange(0, self.w_tilde)  # [0,1,2,...,w-1] (w)

        # creating ranges used to build the sparse linear system and to update it
        self.range_x1 = range(0, self.h_tilde)
        self.range_x2 = range(0, self.w_tilde)

        # initializing the potential at time t-1, using function I
        xx1 = np.reshape(self.x1, (self.h_tilde, 1))  # (h,1)
        xx2 = np.reshape(self.x2, (1, self.w_tilde))  # (1,w)
        if self.I is None:
            self.I = lambda a, b: np.zeros((b.shape[0], a.shape[1]))
        self.u_1 = self.I(xx2, xx1)

        # initializing the velocity-related potential, using function w_V
        if self.method == 'wave':
            if self.w_V is None:
                self.w_V = lambda a, b: np.zeros((b.shape[0], a.shape[1]))
            self.v = self.w_V(xx2, xx1)
            if self.__wave_dissipation_function is None or self.__wave_dissipation_function == 0:
                self.wave_dissipation_function = lambda a, b: np.zeros((b.shape[0], a.shape[1]))
            self.wave_dissipation_function_a = np.zeros((xx1.shape[0], xx2.shape[1]))
            for j in self.range_x2:  # not the same of V_a  in order to allow for piecewise functions
                for i in self.range_x1:
                    self.wave_dissipation_function_a[i, j] = self.__wave_dissipation_function(j, i)

            # moving to scalars, when possible (easier to broadcast)
            if self.pytorch_friendly:
                if np.max(self.v) == np.min(self.v):
                    self.v = np.array(self.v[0][0])
                if np.max(self.wave_dissipation_function_a) == np.min(self.wave_dissipation_function_a):
                    self.wave_dissipation_function_a = np.array(self.wave_dissipation_function_a[0][0])

        # from (j,i) to single integer index
        self.m = lambda j, i: i * self.w_tilde + j

        # initializing data for implicit method in pure DIFFUSION equation (theta scheme)
        if self.method == 'diffusion':

            # defining matrix A (Ax=b)
            # A has 5 diagonal stripes
            N = self.w * self.h
            main = np.zeros(N)  # diagonal
            lower = np.zeros(N - 1)  # sub-diagonal
            upper = np.zeros(N - 1)  # super-diagonal
            lower2 = np.zeros(N - self.w)  # lower diagonal
            upper2 = np.zeros(N - self.w)  # upper diagonal

            # defining vector b (Ax=b)
            self.b = np.zeros(N)  # right-hand side

            # boundary line i=0
            i = 0
            main[self.m(0, i):self.m(self.w, i)] = 1

            # interior mesh lines i=1,...,h-2
            lower_offset = 1
            lower2_offset = self.w
            for i in self.range_x1[1:-1]:
                # boundary
                j = 0
                main[self.m(j, i)] = 1

                # boundary
                j = self.w - 1
                main[self.m(j, i)] = 1

                # interior i points: j=1,...,w-2
                lower2[self.m(1, i) - lower2_offset:self.m(self.w - 1, i) - lower2_offset] = -self.p[
                    'd_theta'] * self.Cy
                lower[self.m(1, i) - lower_offset:self.m(self.w - 1, i) - lower_offset] = -self.p['d_theta'] * self.Cx
                main[self.m(1, i):self.m(self.w - 1, i)] = 1 + 2 * self.p['d_theta'] * (self.Cx + self.Cy)
                upper[self.m(1, i):self.m(self.w - 1, i)] = - self.p['d_theta'] * self.Cx
                upper2[self.m(1, i):self.m(self.w - 1, i)] = - self.p['d_theta'] * self.Cy

            # boundary line i=h-1
            i = self.h - 1
            main[self.m(0, i):self.m(self.w, i)] = 1

            # creating A
            self.A = scipy.sparse.diags(
                diagonals=[main, lower, upper, lower2, upper2],
                offsets=[0, -lower_offset, lower_offset,
                         -lower2_offset, lower2_offset],
                shape=(N, N), format='csc')

        # initializing data for implicit method for WAVE equation (IMPLICIT)
        if self.method == 'wave' and self.w_eval == 'implicit':

            # shortcuts
            w_gamma = self.p['w_gamma']
            # w_absorbing = self.p['w_absorbing']

            # defining matrix A (Ax=b) (actually two instances of A - one is for the first call to update only)
            # A has 5 diagonal stripes
            N = self.w_tilde * self.h_tilde
            main = np.zeros(N)  # diagonal
            main_first_time_step = np.zeros(N)
            lower = np.zeros(N - 1)  # sub-diagonal
            upper = np.zeros(N - 1)  # super-diagonal
            lower2 = np.zeros(N - self.w_tilde)  # lower diagonal
            upper2 = np.zeros(N - self.w_tilde)  # upper diagonal

            # defining vector b (Ax=b)
            self.b = np.zeros(N)  # right-hand side

            # boundary line i=0
            i = 0
            main[self.m(0, i):self.m(self.w_tilde, i)] = 1
            main_first_time_step[self.m(0, i):self.m(self.w_tilde, i)] = 1

            # interior mesh lines i=1,...,h-2
            lower_offset = 1
            lower2_offset = self.w_tilde
            for i in self.range_x1[1:-1]:

                # boundary
                j = 0
                main[self.m(j, i)] = 1
                main_first_time_step[self.m(j, i)] = 1

                # boundary
                j = self.w_tilde - 1
                main[self.m(j, i)] = 1
                main_first_time_step[self.m(j, i)] = 1

                # interior i points: j=1,...,w-2
                lower2[self.m(1, i) - lower2_offset:self.m(self.w_tilde - 1, i) - lower2_offset] = \
                    -(self.c * self.delta_t) ** 2
                lower[self.m(1, i) - lower_offset:self.m(self.w_tilde - 1, i) - lower_offset] = \
                    -(self.c * self.delta_t) ** 2

                for j in range(1, self.w_tilde - 1):
                    main[self.m(j, i)] = w_gamma + self.wave_dissipation_function_a[i, j] * self.delta_t \
                                         + 4 * (self.c * self.delta_t) ** 2
                    main_first_time_step[self.m(j, i)] = 2 * w_gamma \
                                                         + self.wave_dissipation_function_a[i, j] * self.delta_t \
                                                         + 4 * (self.c * self.delta_t) ** 2

                # TODO Lapo, can we remove these commented lines?
                # main[self.m(1, i):self.m(self.w - 1, i)] = w_gamma + w_absorbing * self.delta_t \
                #                                            + 4 * (self.c * self.delta_t) ** 2
                # main_first_time_step[self.m(1, i):self.m(self.w - 1, i)] = 2 * w_gamma + w_absorbing * self.delta_t \
                #                                                            + 4 * (self.c * self.delta_t) ** 2

                upper[self.m(1, i):self.m(self.w_tilde - 1, i)] = -(self.c * self.delta_t) ** 2
                upper2[self.m(1, i):self.m(self.w_tilde - 1, i)] = -(self.c * self.delta_t) ** 2

            # boundary line i=h-1
            i = self.h_tilde - 1
            main[self.m(0, i):self.m(self.w_tilde, i)] = 1  # Boundary line
            main_first_time_step[self.m(0, i):self.m(self.w_tilde, i)] = 1  # Boundary line

            # creating A
            self.A = scipy.sparse.diags(
                diagonals=[main, lower, upper, lower2, upper2],
                offsets=[0, -lower_offset, lower_offset,
                         -lower2_offset, lower2_offset],
                shape=(N, N), format='csc')

            # creating A "for the first call to update only"
            # it differs from A because of the main diagonal
            self.A_first_update = scipy.sparse.diags(
                diagonals=[main_first_time_step, lower, upper, lower2, upper2],
                offsets=[0, -lower_offset, lower_offset,
                         -lower2_offset, lower2_offset],
                shape=(N, N), format='csc')

    @staticmethod
    def __evaluate_gravitational_force(y, u_interpolator, epsilon=0.5, force_scale=100):
        """Evaluate the gravitational force at the FOA coordinates (derivative of the potential term)."""

        if not torch.is_tensor(u_interpolator):
            force_x1 = -(1 / epsilon) * (
                    u_interpolator(y[0] + epsilon / 2, y[1]) - u_interpolator(y[0] - epsilon / 2, y[1]))
            force_x2 = -(1 / epsilon) * (
                    u_interpolator(y[0], y[1] + epsilon / 2) - u_interpolator(y[0], y[1] - epsilon / 2))
            force_x1 = force_scale * force_x1.item(0)
            force_x2 = force_scale * force_x2.item(0)
        else:
            force_x1 = -(1 / epsilon) * (
                    u_interpolator[int(y[0] + epsilon / 2), int(y[1])] - u_interpolator[
                int(y[0] - epsilon / 2), int(y[1])])
            force_x2 = -(1 / epsilon) * (
                    u_interpolator[int(y[0]), int(y[1] + epsilon / 2)] - u_interpolator[
                int(y[0]), int(y[1] - epsilon / 2)])
            force_x1 = force_scale * force_x1.item()
            force_x2 = force_scale * force_x2.item()
        return force_x1, force_x2

    def __update_position_and_velocity(self, u_interpolator, lock=None):
        """Update position of the FOA coordinates and the velocity of the FOA."""

        if lock is not None:
            with lock:
                pos_and_vel = odeint(GEymolLoc.__my_ode, self.y,
                                     np.linspace(self.t, self.t + self.delta_t, 10),  # 10 time instants
                                     args=(u_interpolator, self.p['epsilon'],
                                           self.p['force_scale'], self.p['dissipation'],
                                           self.h, self.w, self.w_abs_layer),
                                     mxstep=0, rtol=0.1, atol=0.1)
                pos_and_vel = pos_and_vel[-1, :]  # picking up the latest integrated time instant
        else:
            pos_and_vel = odeint(GEymolLoc.__my_ode, self.y,
                                 np.linspace(self.t, self.t + self.delta_t, 10),  # 10 time instants
                                 args=(u_interpolator, self.p['epsilon'],
                                       self.p['force_scale'], self.p['dissipation'],
                                       self.h, self.w, self.w_abs_layer),
                                 mxstep=0, rtol=0.1, atol=0.1)
            pos_and_vel = pos_and_vel[-1, :]  # picking up the latest integrated time instant

        return pos_and_vel

    @staticmethod
    def __my_ode(y, t, u_interpolator, epsilon, force_scale, dissipation, h, w, w_abs_layer):
        """ODE that has to be integrated to update FOA."""

        GEymolLoc.__stay_inside_fix_nans(y, (h, w), w_abs_layer)

        force_x1, force_x2 = GEymolLoc.__evaluate_gravitational_force(y, u_interpolator,
                                                                      epsilon=epsilon,
                                                                      force_scale=force_scale)

        gravitational_force = np.array([force_x1, force_x2])
        dy = np.concatenate([np.array(y[2:]),
                             gravitational_force - dissipation * np.array(y[2:])])
        return dy

    def __update_potential_wave_explicit_1(self, call1=False):
        """Method that updates the potential term in the case of wave equation (explicit).
        Central difference approximation for the first order time derivative. In this case, w_gamma can not be null."""
        # shortcuts
        w_gamma = self.p['w_gamma']
        if w_gamma == 0:
            exit(" With the 'explicit1' method, w_gamma can not be null!")

        if self.pytorch_friendly:

            u_xx_plus_u_yy_scaled_by_Cx2_Cy2 = torch.conv2d(
                self.u_1.view(1, 1, self.h + 2 * self.w_abs_layer, self.w + 2 * self.w_abs_layer),
                self.conv2d_derivative, padding=1).view(self.h + 2 * self.w_abs_layer, self.w + 2 * self.w_abs_layer)
            if call1:
                self.u = (w_gamma * self.u_1 + self.delta_t * self.v * \
                          (w_gamma - (self.wave_dissipation_function_a * self.delta_t / 2)) + \
                          u_xx_plus_u_yy_scaled_by_Cx2_Cy2 / 2.0 + \
                          (self.delta_t ** 2) / 2 * self.source) / w_gamma
            else:
                self.u = (2 * w_gamma * self.u_1 -
                          (w_gamma - (self.wave_dissipation_function_a * self.delta_t) / 2) * self.u_2 + \
                          u_xx_plus_u_yy_scaled_by_Cx2_Cy2 + (self.delta_t ** 2) * self.source) / \
                         (w_gamma + (self.wave_dissipation_function_a * self.delta_t) / 2)

        else:
            twice_u_1_no_boundaries = 2 * self.u_1[1:-1, 1:-1]
            u_yy = self.u_1[:-2, 1:-1] - twice_u_1_no_boundaries + self.u_1[2:, 1:-1]  # (h-2) x (w-2)
            u_xx = self.u_1[1:-1, :-2] - twice_u_1_no_boundaries + self.u_1[1:-1, 2:]  # (h-2) x (w-2)

            if call1:
                self.u[1:-1, 1:-1] = (w_gamma * self.u_1[1:-1, 1:-1] + self.delta_t * self.v[1:-1, 1:-1] * \
                                      (w_gamma - (self.wave_dissipation_function_a[1:-1, 1:-1] * self.delta_t / 2)) + \
                                      self.Cx2 / 2 * u_xx + self.Cy2 / 2 * u_yy + \
                                      (self.delta_t ** 2) / 2 * self.source[1:-1, 1:-1]) / w_gamma

            else:
                self.u[1:-1, 1:-1] = (2 * w_gamma * self.u_1[1:-1, 1:-1] -
                                      (w_gamma - (self.wave_dissipation_function_a[1:-1, 1:-1] * self.delta_t) / 2) * \
                                      self.u_2[1:-1, 1:-1] + self.Cx2 * u_xx + self.Cy2 * u_yy +
                                      (self.delta_t ** 2) * self.source[1:-1, 1:-1]) / \
                                     (w_gamma + (self.wave_dissipation_function_a[1:-1, 1:-1] * self.delta_t) / 2)

        # boundary conditions (zeroing border coordinates)
        j = 0
        self.u[:, j] = 0

        j = self.w + 2 * self.w_abs_layer - 1
        self.u[:, j] = 0

        i = 0
        self.u[i, :] = 0

        i = self.h + 2 * self.w_abs_layer - 1
        self.u[i, :] = 0

    def __update_potential_wave_explicit_2(self, call1=False):
        """Method that updates the potential term in the case of wave equation (explicit).
        Forward difference approximation for the first order time derivative. In this case, w_gamma can also be null."""
        # shortcuts
        w_gamma = self.p['w_gamma']

        if self.pytorch_friendly:
            u_xx_plus_u_yy_scaled_by_Cx2_Cy2 = torch.conv2d(
                self.u_1.view(1, 1, self.h + 2 * self.w_abs_layer, self.w + 2 * self.w_abs_layer),
                self.conv2d_derivative, padding=1).view(self.h + 2 * self.w_abs_layer, self.w + 2 * self.w_abs_layer)

            if call1:
                self.u = ((2 * w_gamma + self.wave_dissipation_function_a * self.delta_t) * self.u_1 + \
                          2 * w_gamma * self.delta_t * self.v + u_xx_plus_u_yy_scaled_by_Cx2_Cy2 + \
                          (self.delta_t ** 2) * self.source) / \
                         (2 * w_gamma + self.wave_dissipation_function_a * self.delta_t)
            else:
                self.u = ((2 * w_gamma + self.wave_dissipation_function_a * self.delta_t) * self.u_1 - \
                          w_gamma * self.u_2 + \
                          u_xx_plus_u_yy_scaled_by_Cx2_Cy2 + (self.delta_t ** 2) * self.source) / \
                         (w_gamma + (self.wave_dissipation_function_a * self.delta_t))
        else:
            twice_u_1_no_boundaries = 2 * self.u_1[1:-1, 1:-1]
            u_yy = self.u_1[:-2, 1:-1] - twice_u_1_no_boundaries + self.u_1[2:, 1:-1]  # (h-2) x (w-2)
            u_xx = self.u_1[1:-1, :-2] - twice_u_1_no_boundaries + self.u_1[1:-1, 2:]  # (h-2) x (w-2)

            if call1:
                self.u[1:-1, 1:-1] = ((2 * w_gamma + self.wave_dissipation_function_a[1:-1,
                                                     1:-1] * self.delta_t) * self.u_1[1:-1, 1:-1] + \
                                      2 * w_gamma * self.delta_t * self.v[1:-1, 1:-1] + \
                                      self.Cx2 * u_xx + self.Cy2 * u_yy + \
                                      (self.delta_t ** 2) * self.source[1:-1, 1:-1]) / \
                                     (2 * w_gamma + self.wave_dissipation_function_a[1:-1, 1:-1] * self.delta_t)

            else:
                self.u[1:-1, 1:-1] = ((2 * w_gamma + self.wave_dissipation_function_a[1:-1,
                                                     1:-1] * self.delta_t) * self.u_1[1:-1, 1:-1] - \
                                      w_gamma * self.u_2[1:-1, 1:-1] + \
                                      self.Cx2 * u_xx + self.Cy2 * u_yy + (self.delta_t ** 2) * self.source[1:-1,
                                                                                                1:-1]) / \
                                     (w_gamma + self.wave_dissipation_function_a[1:-1, 1:-1] * self.delta_t)

        # boundary conditions (zeroing border coordinates)
        j = 0
        self.u[:, j] = 0

        j = self.w + 2 * self.w_abs_layer - 1
        self.u[:, j] = 0

        i = 0
        self.u[i, :] = 0

        i = self.h + 2 * self.w_abs_layer - 1
        self.u[i, :] = 0

    def __update_potential_wave_implicit(self, call1=False):
        """Method that updates the potential term in the case of wave equation (implicit)."""

        # shortcuts
        w_gamma = self.p['w_gamma']

        # b: boundary indices
        i = 0
        self.b[self.m(0, i):self.m(self.w_tilde, i)] = 0

        for i in self.range_x1[1:-1]:

            # b: boundary indices
            j = 0
            p = self.m(j, i)
            self.b[p] = 0

            # b: boundary indices
            j = self.w_tilde - 1
            p = self.m(j, i)
            self.b[p] = 0

            # b: other coordinates
            jmin = self.range_x2[1]
            jmax = self.range_x2[-1]  # for slice, max j index is self.range_x2[-1]-1
            if call1:
                self.b[self.m(jmin, i):self.m(jmax, i)] = \
                    (2 * w_gamma + self.delta_t * self.wave_dissipation_function_a[i, jmin:jmax]) * self.u_1[i,
                                                                                                    jmin:jmax] + \
                    2 * w_gamma * self.delta_t * self.v[i, jmin:jmax] + \
                    self.delta_t ** 2 * self.source[i, jmin:jmax]
            else:
                self.b[self.m(jmin, i):self.m(jmax, i)] = \
                    (2 * w_gamma + self.delta_t * self.wave_dissipation_function_a[i, jmin:jmax]) * self.u_1[i,
                                                                                                    jmin:jmax] - \
                    w_gamma * self.u_2[i, jmin:jmax] + \
                    self.delta_t ** 2 * self.source[i, jmin:jmax]

        # b: boundary indices
        i = self.h_tilde - 1
        self.b[self.m(0, i):self.m(self.w_tilde, i)] = 0

        # solving system Ax=b
        if call1:
            x = scipy.sparse.linalg.spsolve(self.A_first_update, self.b, permc_spec='MMD_ATA')
        else:
            x = scipy.sparse.linalg.spsolve(self.A, self.b, permc_spec='MMD_ATA')

        # getting back the right-shape potential
        self.u = x.reshape(self.h_tilde, self.w_tilde)

    def __update_potential_diffusion(self):
        """Method that updates the potential term in the case of diffusion equation (implicit only)."""

        # shortcut
        d_theta = self.p['d_theta']

        # b: boundary indices
        i = 0
        self.b[self.m(0, i):self.m(self.w, i)] = 0

        for i in self.range_x1[1:-1]:
            # b: boundary indices
            j = 0
            p = self.m(j, i)
            self.b[p] = 0

            # b: boundary indices
            j = self.w - 1
            p = self.m(j, i)
            self.b[p] = 0

            # b: other coordinates
            jmin = self.range_x2[1]
            jmax = self.range_x2[-1]  # for slice, max j index is Ix2[-1]-1
            self.b[self.m(jmin, i):self.m(jmax, i)] = \
                self.u_1[i, jmin:jmax] + (1 - d_theta) * \
                (self.Cx * (self.u_1[i, jmin + 1:jmax + 1] - 2 * self.u_1[i, jmin:jmax]
                            + self.u_1[i, jmin - 1:jmax - 1])
                 + self.Cy * (self.u_1[i + 1, jmin:jmax] - 2 * self.u_1[i, jmin:jmax] + self.u_1[i - 1, jmin:jmax])) + \
                d_theta * self.delta_t * self.source[i, jmin:jmax] + \
                (1 - d_theta) * self.delta_t * self.source_1[i, jmin:jmax]

        # b: boundary indices
        i = self.h - 1
        self.b[self.m(0, i):self.m(self.w, i)] = 0

        # solving system Ax=b
        x = scipy.sparse.linalg.spsolve(self.A, self.b, permc_spec='MMD_ATA')

        # getting back the right-shape potential
        self.u = x.reshape(self.h, self.w)

    def __update_face_map(self, frame, updating_factor=.3, frame_gray_uint8_cpu=None):
        """Update the face mp with the face(s) detected in the current frame.
        Passing frame_gray_uint8_cpu, if available, will speedup the computations.
        """

        if frame_gray_uint8_cpu is not None:
            faces = self.face_detector.detectMultiScale(frame_gray_uint8_cpu)
        else:
            if frame.dtype == torch.float32:
                faces = self.face_detector.detectMultiScale((frame * 255.0).cpu().numpy().astype(np.uint8), 1.3, 5)
            else:
                raise ValueError("Unsupported tensor type (expected torch.float32, values in [0,1]).")

        face_map_new = torch.zeros_like(self.face_map)
        for (y, x, h, w) in faces:
            face_map_new[x:x + w, y:y + h] = 1.0  # tested

        # update as weighted sum
        self.face_map = (1.0 - updating_factor) * self.face_map + updating_factor * face_map_new

    def __update_inhibition_of_return(self, ior_matrix, row_col):
        """Update the IOR matrix using a Gaussian function centered in the given coordinates."""

        beta = 0.1 / self.delta_t
        sigma2 = 3650

        def generate_gaussian(y, h, w, var2):
            yy = self.X1torch - y[0]  # rows (float32)
            xx = self.X2torch - y[1]  # columns (float32)
            return torch.exp(-((xx * xx + yy * yy) / var2))

        return (1 - self.delta_t * beta) * ior_matrix \
               + (self.delta_t * beta) * generate_gaussian(row_col, self.h, self.w, sigma2)

    @staticmethod
    def __stay_inside_fix_nans(y, frame_hw, w_abs_layer, ray=5):
        row, col, vrow, vcol = y

        if isnan(row) or isnan(col):
            row, col = 0, 0

        if row - ray < w_abs_layer:
            row = ray + w_abs_layer
        elif row + ray > frame_hw[0] + w_abs_layer - 1:
            row = frame_hw[0] + w_abs_layer - 1 - ray

        if col - ray < w_abs_layer:
            col = w_abs_layer + ray
        elif col + ray > frame_hw[1] + w_abs_layer - 1:
            col = frame_hw[1] + w_abs_layer - 1 - ray

        y[0] = row
        y[1] = col

        if isnan(vrow) or isnan(vcol):
            y[2] = 0.0
            y[3] = 0.0

    @staticmethod
    def __stay_inside_fix_nans_round_to_int(y, frame_hw, w_abs_layer, ray=5):
        row, col, vrow, vcol = y

        if isnan(row) or isnan(col):
            row, col = 0, 0
        else:
            row, col = int(round(row)), int(round(col))

        if row - ray < w_abs_layer:
            row = w_abs_layer + ray
        elif row + ray > frame_hw[0] + w_abs_layer - 1:
            row = frame_hw[0] + w_abs_layer - 1 - ray

        if col - ray < w_abs_layer:
            col = w_abs_layer + ray
        elif col + ray > frame_hw[1] + w_abs_layer - 1:
            col = frame_hw[1] + w_abs_layer - 1 - ray

        y[0] = row
        y[1] = col

        if isnan(vrow) or isnan(vcol):
            y[2] = 0.0
            y[3] = 0.0

    @staticmethod
    def __get_sobel_kernel(device):
        kernel = torch.tensor([[[[-1., -4., -6., -4., -1.],
                                 [-2., -8., -12., -8., -2.],
                                 [0., 0., 0., 0., 0.],
                                 [2., 8., 12., 8., 2.],
                                 [1., 4., 6., 4., 1.]]],
                               [[[-1., -2., 0., 2., 1.],
                                 [-4., -8., 0., 8., 4.],
                                 [-6., -12., 0., 12., 6.],
                                 [-4., -8., 0., 8., 4.],
                                 [-1., -2., 0., 2., 1.]]]]
                              , dtype=torch.float32, device=device)  # 2 x 1 x 5 x 5
        return kernel

    @staticmethod
    def __get_gradient_norm(frame, sobel_kernel):
        sobel_xy = torch.nn.functional.conv2d(frame, sobel_kernel, bias=None, padding=(2, 2), groups=1)

        # getting norm
        grad_norm = torch.squeeze(torch.sqrt(torch.sum(sobel_xy ** 2, dim=1)))  # h x w

        return grad_norm

    @staticmethod
    def __get_opticalflow_norm(of, normalize=False):

        # getting norm (optical flow is expected to be 1 x 2 x h x w)
        of_norm = torch.squeeze(torch.sqrt(torch.sum(of ** 2, dim=1)))  # h x w

        # get outliers (it solves ego-motion)
        of_norm = of_norm - torch.mean(of_norm)
        of_norm = torch.abs(of_norm)

        return of_norm

    @staticmethod
    def __normalize(data):
        max_val = torch.max(data)
        if max_val > 0.0:
            return data / max_val
        else:
            return data

    @staticmethod
    def __get_virtualmass_norm(frame, xy):
        z = torch.zeros((frame.shape[2], frame.shape[3]), dtype=torch.float, device=frame.device)
        z[int(xy[0]), int(xy[1])] = 1.0
        return z

    def __initialize_plot_data(self):
        """Initialize those fields that will be needed to create the debug plots.
        There are 4 plot types: 1 (simple), 2 (3d plot), 3 (heat map), 4 (heat map with details and motion).
        """
        assert self.plot_method == 1 or self.plot_method == 2 or self.plot_method == 3 or self.plot_method == 4

        # vmin1 and vmax1 normalize the potential's color range between vmin1 and vmax1 if they are given.
        self.vmin1 = self.parameters['vmin1'] if 'vmin1' in self.parameters else None
        self.vmax1 = self.parameters['vmax1'] if 'vmax1' in self.parameters else None

        # vmin2 and vmax2 normalize the density mass's color range between vmin2 and vmax2 in 'plot_method = 4'.
        self.vmin2 = self.parameters['vmin2'] if 'vmin2' in self.parameters else -10
        self.vmax2 = self.parameters['vmax2'] if 'vmax2' in self.parameters else 0

        # Changing backend to save plots in an efficient way.
        if self.save_plot:
            matplotlib.use('agg')

        self.plot_counter = 0  # it counts the frame index in order to create a video of the plots
        self.dpi = 100
        self.fig_size = 1100 / float(self.dpi), 800 / float(self.dpi)  # (width / dpi, height / dpi)

        if self.plot_method == 1 or self.plot_method == 2:
            self.fig = plt.figure(figsize=self.fig_size)
            self.ax = self.fig.add_subplot(111, projection='3d')
            self.ax.set_zlim(-1, 1)
            self.ax_figure = None

            self.ax_details = None
            self.ax_motion = None

            # axes and color-bar
            self.cax = self.fig.add_axes([0.05, 0.15, 0.05, 0.7])
            sm = plt.cm.ScalarMappable(cmap='inferno', norm=plt.Normalize(vmin=-1, vmax=1))
            plt.colorbar(sm, shrink=0.5, cax=self.cax)

            # mesh for plot
            self.X1, self.X2 = np.meshgrid(self.x1, self.x2, indexing='ij')
        elif self.plot_method == 3:
            self.fig_size = 1100 / float(self.dpi), 400 / float(self.dpi)  # (width / dpi, height / dpi)
            self.fig = plt.figure(figsize=self.fig_size, constrained_layout=True)
            spec = gridspec.GridSpec(ncols=3, nrows=1, figure=self.fig, width_ratios=[0.1, 1, 1], height_ratios=[1])
            self.ax = self.fig.add_subplot(spec[0, 1])
            self.ax_figure = self.fig.add_subplot(spec[0, 2])

            self.ax_details = None
            self.ax_motion = None

            # axes and color-bar
            self.ax.xaxis.set_visible(False)
            self.ax.yaxis.set_visible(False)
            self.ax_figure.xaxis.set_visible(False)
            self.ax_figure.yaxis.set_visible(False)
            self.cax = self.fig.add_subplot(spec[0, 0])
            if self.vmin1 is not None and self.vmax1 is not None:
                sm = plt.cm.ScalarMappable(cmap='inferno', norm=plt.Normalize(vmin=self.vmin1, vmax=self.vmax1))
                self.fig.colorbar(sm, shrink=0.5, cax=self.cax)

            # mesh for plot
            self.X1, self.X2 = np.meshgrid(self.x1, self.x2, indexing='ij')
        elif self.plot_method == 4:
            self.fig = plt.figure(figsize=self.fig_size, constrained_layout=True)
            spec = gridspec.GridSpec(ncols=3, nrows=2, figure=self.fig, width_ratios=[0.1, 1, 1], height_ratios=[1, 1])
            self.ax = self.fig.add_subplot(spec[0, 1])
            self.ax_figure = self.fig.add_subplot(spec[0, 2])

            self.ax_details = self.fig.add_subplot(spec[1, 1])
            self.ax_motion = self.fig.add_subplot(spec[1, 2])

            # axes and color-bar
            self.ax.xaxis.set_visible(False)
            self.ax.yaxis.set_visible(False)
            self.ax_figure.xaxis.set_visible(False)
            self.ax_figure.yaxis.set_visible(False)
            self.ax_details.xaxis.set_visible(False)
            self.ax_details.yaxis.set_visible(False)
            self.ax_motion.xaxis.set_visible(False)
            self.ax_motion.yaxis.set_visible(False)

            self.cax = self.fig.add_subplot(spec[0, 0])
            if self.vmin1 is not None and self.vmax1 is not None:
                sm = plt.cm.ScalarMappable(cmap='inferno', norm=plt.Normalize(vmin=self.vmin1, vmax=self.vmax1))
                self.fig.colorbar(sm, shrink=0.5, cax=self.cax)

            self.cax2 = self.fig.add_subplot(spec[1, 0])
            self.cax2.set_aspect(6)
            sm = plt.cm.ScalarMappable(cmap='viridis', norm=plt.Normalize(vmin=self.vmin2, vmax=self.vmax2))
            self.fig.colorbar(sm, shrink=0.5, cax=self.cax2, format='%.2f')

            # mesh for plot
            self.X1, self.X2 = np.meshgrid(self.x1, self.x2, indexing='ij')

    @staticmethod
    def __plot(u, u_interpolator, X1, X2, t, pos_x1, pos_x2,
               frame, fig, ax, ax_figure, cax, w_abs_layer,
               h, w, counter, dpi, vmin1, vmax1, vmin2, vmax2, plot_method, save_plot,
               ax_details, gradient_norm_t, ax_motion, of_norm_t):
        """Plot the current status of the model, for debug purposes.
        This method is static for making it easier to handle, even if it would be less verbose to
        remove the static property.
        """
        assert plot_method == 1 or plot_method == 2 or plot_method == 3 or plot_method == 4

        # removing previously created plot files
        if save_plot and t == 0:
            shutil.rmtree('plot', ignore_errors=True)
            os.mkdir('plot')

        resolution = 5

        # prepare plots that will be saved to disk as video frames
        if save_plot is True and plot_method != 3 and plot_method != 4:
            if counter <= 90:
                ax.view_init(elev=15., azim=counter - 90)
            elif 90 < counter <= 270:
                ax.view_init(elev=15., azim=-1 * (counter - 90))
            elif 270 < counter:
                ax.view_init(elev=15., azim=(counter - 450))
            elif counter == 631:  # stop the run after that
                exit()
            resolution = 3  # higher resolution in 3d plots

        # converting to NumPy and creating a fake RGB image
        frame = np.squeeze(frame.numpy())
        frame = np.pad(frame, ((w_abs_layer, w_abs_layer), (w_abs_layer, w_abs_layer)))
        frame = np.stack((frame, frame, frame), axis=2)

        if plot_method == 4:
            gradient_norm_t = gradient_norm_t.numpy()
            of_norm_t = of_norm_t.numpy()

            gradient_norm_t = np.pad(gradient_norm_t, ((w_abs_layer, w_abs_layer), (w_abs_layer, w_abs_layer)))
            if of_norm_t.shape != ():
                of_norm_t = np.pad(of_norm_t, ((w_abs_layer, w_abs_layer), (w_abs_layer, w_abs_layer)))
            else:
                of_norm_t = np.zeros((h + 2 * w_abs_layer, w + 2 * w_abs_layer))

        # if t == 0:
        vec_start_x = [w_abs_layer, w_abs_layer, w_abs_layer, w + w_abs_layer - 1]
        vec_start_y = [w_abs_layer, w_abs_layer, h + w_abs_layer - 1, w_abs_layer]
        vec_start_z = [0, 0, 0, 0]
        vec_end_x = [w + w_abs_layer - 1, w_abs_layer, w + w_abs_layer - 1, w + w_abs_layer - 1]
        vec_end_y = [w_abs_layer, h + w_abs_layer - 1, h + w_abs_layer - 1, h + w_abs_layer - 1]
        vec_end_z = [0, 0, 0, 0]

        # **** PLOT METHODS 1 and 2
        if plot_method == 1 or plot_method == 2:

            title = ax.text(0, 1.5, 1.5, 't = %f ' % t)

            if plot_method == 1:
                cax.clear()
                cax.set_axis_off()
                surf = ax.plot_wireframe(X2, -X1, u, color='black')
                point = ax.scatter(pos_x2, -pos_x1, u_interpolator(pos_x1, pos_x2), color='red')
                frame = ax.plot_surface(X2, -X1, np.atleast_2d(-1), rstride=5, cstride=5, facecolors=frame, alpha=.3)

            elif plot_method == 2:
                surf = ax.plot_surface(X2, -X1, u, cmap='inferno', edgecolor='none', shade=False, vmin=-2, vmax=0,
                                       alpha=.7)
                point = ax.scatter(pos_x2, -pos_x1, u_interpolator(pos_x1, pos_x2), color='red')
                frame = ax.plot_surface(X2, -X1, np.atleast_2d(-1), rstride=resolution, cstride=resolution,
                                        facecolors=frame, alpha=.3)

            if t == 0:
                for i in range(4):
                    ax.plot([vec_start_x[i], vec_end_x[i]], [-vec_start_y[i], -vec_end_y[i]],
                            zs=[vec_start_z[i], vec_end_z[i]], color='black')

            # pause between frames
            time.sleep(0.)

            # saving to disk
            if save_plot:
                filename_to_save = os.path.join("plot", f"{counter}.png")
                plt.savefig(filename_to_save, dpi=dpi, facecolor='w', edgecolor='w',
                            orientation='portrait', quality=95)

            # drawing to screen
            if not save_plot:
                plt.draw()
                plt.pause(0.00001)
            surf.remove()
            point.remove()
            frame.remove()
            title.set_text('')

        # **** PLOT METHOD 3 (HEAT MAP)
        if plot_method == 3:
            vis_frame = ax_figure.imshow(frame, cmap=None)
            point2 = ax_figure.scatter(pos_x2, pos_x1, color='red')

            ax.text(10, - h - 2 * w_abs_layer + 10, 't = %0.2f s ' % t)
            N = 50

            if vmin1 is None and vmax1 is None:
                u_min = float(torch.min(u))
                potential = ax.contourf(X2, -X1, u, N, cmap='inferno', extend='neither', vmin=u_min, vmax=0)
                cax.set_aspect(6)
                ls = np.linspace(u_min, 0, 5, endpoint=True)
                cbar = fig.colorbar(potential, cax=cax, format='%.2f', spacing='uniform',ticks=ls)
            else:
                ax.contourf(X2, -X1, u, N, cmap='inferno', vmin=vmin1, vmax=vmax1)

            ax.set_aspect('equal')
            point1 = ax.scatter(pos_x2, -pos_x1, color='red')

            for i in range(4):
                ax.plot([vec_start_x[i], vec_end_x[i]], [-vec_start_y[i], -vec_end_y[i]], color='black')

            # pause between frames
            time.sleep(0.)

            # saving to disk
            if save_plot:
                filename_to_save = os.path.join("plot", f"{counter}.png")
                plt.savefig(filename_to_save, dpi=dpi, facecolor='w', edgecolor='w',
                            orientation='portrait', quality=95)

            # drawing to screen
            if not save_plot:
                plt.draw()
                plt.pause(0.00001)
            ax.clear()
            if vmin1 is None and vmax1 is None:
                cax.clear()
            point1.remove()
            vis_frame.remove()
            point2.remove()
            # fig.suptitle('', fontsize=16)
            # ax.text(10, -h+10, '')

        # **** PLOT METHOD 4 (HEAT MAP + masses)
        if plot_method == 4:
            vis_frame = ax_figure.imshow(frame, cmap=None)
            point2 = ax_figure.scatter(pos_x2, pos_x1, color='red')

            text = ax.text(10, - h - 2 * w_abs_layer + 10, 't = %0.2f s ' % t)
            if t == 0:
                ax.text(10, -25, "Potential")

            N = 50
            if vmin1 is None and vmax1 is None:
                u_min = float(torch.min(u))
                potential = ax.contourf(X2, -X1, u, N, cmap='inferno', extend='neither', vmin=u_min, vmax=0)
                cax.set_aspect(6)
                ls = np.linspace(u_min, 0, 5, endpoint=True)
                cbar = fig.colorbar(potential, cax=cax, format='%.2f', spacing='uniform',ticks=ls)
            else:
                ax.contourf(X2, -X1, u, N, cmap='inferno', vmin=vmin1, vmax=vmax1)

            ax.set_aspect('equal')
            point1 = ax.scatter(pos_x2, -pos_x1, color='red')

            if t == 0:
                ax_details.text(10, -25, "Details (including IOR)")
            ax_details.contourf(X2, -X1, gradient_norm_t, N, cmap='viridis', vmin=vmin2, vmax=vmax2)
            ax_details.set_aspect('equal')
            point3 = ax_details.scatter(pos_x2, -pos_x1, color='red')

            if t == 0:
                ax_motion.text(10, -25, "Motion")
            ax_motion.contourf(X2, -X1, of_norm_t, N, cmap='viridis', vmin=vmin2, vmax=vmax2)
            ax_motion.set_aspect('equal')
            point4 = ax_motion.scatter(pos_x2, -pos_x1, color='red')

            for i in range(4):
                ax.plot([vec_start_x[i], vec_end_x[i]], [-vec_start_y[i], -vec_end_y[i]], color='black')
                ax_details.plot([vec_start_x[i], vec_end_x[i]], [-vec_start_y[i], -vec_end_y[i]], color='black')
                ax_motion.plot([vec_start_x[i], vec_end_x[i]], [-vec_start_y[i], -vec_end_y[i]], color='black')

            # pause between frames
            # time.sleep(0.)

            # saving to disk
            if save_plot:
                filename_to_save = os.path.join("plot", f"{counter}.png")
                plt.savefig(filename_to_save, dpi=dpi, facecolor='w', edgecolor='w',
                            orientation='portrait', quality=95)

            # drawing to screen
            if not save_plot:
                plt.draw()
                plt.pause(0.00001)
            ax.clear()
            ax_details.clear()
            ax_motion.clear()
            if vmin1 is None and vmax1 is None:
                cax.clear()
            text.remove()
            point1.remove()
            vis_frame.remove()
            point2.remove()
            point3.remove()
            point4.remove()
