import torch
import numpy as np
import os
import cv2
import time

device_cpu = torch.device("cpu")  # default PyTorch device


def prepare_img(img_cv, w, h, device):
    """Convert the given OpenCV image to the target size and format, returning its OpenCV and PyTorch instances."""

    if img_cv is not None:
        img_cv = resize_and_grayscale(img_cv, w, h)
        img_torch = np_uint8_to_torch_float_01(img_cv, device=torch.device(device))
        return img_cv, img_torch
    else:
        return None, None


def get_frame_from_video(video_capture, w, h, device, infinite=False):
    """Get the next frame."""

    start_time = time.time()
    ret_val, img_cv = video_capture.read()
    if not ret_val or img_cv is None:
        if infinite:
            reset_video(video_capture)
            ret_val, img_cv = video_capture.read()
        if not ret_val or img_cv is None:
            return None, None, time.time() - start_time
        else:
            img_cv, img_torch = prepare_img(img_cv, w, h, device)
            return img_cv, img_torch, time.time() - start_time
    else:
        img_cv, img_torch = prepare_img(img_cv, w, h, device)
        return img_cv, img_torch, time.time() - start_time


def draw_foa(img, foa, external_layer, sac):
    """Draw the given image, showing a red dot at the FOA coordinates or a blue dot in case of saccades."""
    w = img.shape[1]
    h = img.shape[0]

    # copying source image
    img_to_draw = np.concatenate([img, img, img], axis=2)

    # drawing red/blue square
    img_to_draw[max(int(foa[0]) - external_layer - 5, 0):min(int(foa[0]) - external_layer + 5, h),
                max(int(foa[1]) - external_layer - 5, 0):min(int(foa[1]) - external_layer + 5, w), 0] = \
                0 if not sac else 255
    img_to_draw[max(int(foa[0]) - external_layer - 5, 0):min(int(foa[0]) - external_layer + 5, h),
                max(int(foa[1]) - external_layer  - 5, 0):min(int(foa[1]) - external_layer + 5, w), 1] = 0
    img_to_draw[max(int(foa[0]) - external_layer - 5, 0):min(int(foa[0]) - external_layer + 5, h),
                max(int(foa[1]) - external_layer - 5, 0):min(int(foa[1]) - external_layer + 5, w), 2] = \
                255 if not sac else 0

    # showing on screen
    cv2.imshow('Frame + FOA (red/blue square)', img_to_draw)
    cv2.waitKey(10)


def reset_video(video_capture):
    """Reset an opened video"""
    video_capture.set(cv2.CAP_PROP_POS_FRAMES, 0)


def compute_optical_flow(prev_frame_uint8_gray, cur_frame_uint8_gray, device, prev_optical_flow_float32=None):
    """A wrapper to the OpenCV implementation of the Farneback algorithm for optical flow computation."""

    start_time = time.time()
    if prev_frame_uint8_gray is None:
        of_cv = np.zeros((cur_frame_uint8_gray.shape[0], cur_frame_uint8_gray.shape[1], 2), np.float32)
    else:
        of_cv = cv2.calcOpticalFlowFarneback(cur_frame_uint8_gray, prev_frame_uint8_gray,
                                             prev_optical_flow_float32,
                                             pyr_scale=0.4, levels=5, winsize=12, iterations=10, poly_n=5,
                                             poly_sigma=1.1, flags=cv2.OPTFLOW_USE_INITIAL_FLOW
                                             if prev_optical_flow_float32 is not None else 0)
    computation_time = time.time() - start_time

    start_time = time.time()
    of_torch = np_float32_to_torch_float(of_cv, device=torch.device(device))
    data_management_time = time.time() - start_time

    return of_cv, of_torch, (computation_time, data_management_time)


def resize_and_grayscale(img, w, h):
    """Resize a given image and convert it to grayscale."""
    return np.reshape(cv2.resize(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), (w, h)), (h, w, 1))


def open_stream(input_data):
    """Open an input image, a video, or a stream from a device (web-cam).

    Args:
        input_data (str/int): path to video (str), path to file (str), device ID (int)

    Returns:
        img: the opened image or the first frame of the video.
        video: the reference to the VideoCapture.
        err_msg: a string with the text of the eventually raised error messages (it is None if no errors).
    """
    err_msg = None
    video = None
    img = None
    fps = -1.0
    if not os.path.isfile(input_data) and input_data != 0 and input_data != 1:
        err_msg = "File " + input_data + " does not exist"
    else:
        try:
            img = cv2.imread(str(input_data))
            if img is None:
                raise IOError
        except (ValueError, IOError):
            try:
                video = cv2.VideoCapture(input_data)
                if video is None:
                    raise IOError
                fps = video.get(cv2.CAP_PROP_FPS)
                ret_val, img = video.read()
                if not ret_val or img is None:
                    raise IOError
            except (ValueError, IOError):
                err_msg = "Cannot open the existing file: " + input_data
    if err_msg is not None:
        return None, None, None, err_msg
    else:
        return img, fps, video, None


def torch_float_01_to_np_uint8(torch_img):
    """PyTorch image tensor to NumPy image tensor"""
    return (torch_img * 255.0).cpu().astype(np.uint8)


def np_uint8_to_torch_float_01(numpy_img, device=None):
    """NumPy image tensor to PyTorch image tensor"""

    if numpy_img.ndim == 2:
        h = numpy_img.shape[0]
        w = numpy_img.shape[1]
        if device is None or device == device_cpu:
            return torch.from_numpy(numpy_img).float().div_(255.0).resize_(1, 1, h, w)
        else:
            return torch.from_numpy(numpy_img).float().resize_(1, 1, h, w).to(device).div_(255.0)
    elif numpy_img.ndim == 3:
        if device is None or device == device_cpu:
            return torch.from_numpy(numpy_img.transpose(2, 0, 1)).float().unsqueeze_(0).div_(255.0)
        else:
            return torch.from_numpy(numpy_img.transpose(2, 0, 1)).float().to(device).unsqueeze_(0).div_(255.0)
    elif numpy_img.ndim == 4:
        if device is None or device == device_cpu:
            return torch.from_numpy(numpy_img.transpose(0, 3, 1, 2)).float().div_(255.0)
        else:
            return torch.from_numpy(numpy_img.transpose(0, 3, 1, 2)).float().to(device).div_(255.0)
    else:
        raise ValueError("Unsupported image type.")


def np_float32_to_torch_float(numpy_img, device=None):
    """NumPy image tensor to PyTorch image tensor"""

    if numpy_img.ndim == 2:
        h = numpy_img.shape[0]
        w = numpy_img.shape[1]
        if device is None or device == device_cpu:
            return torch.from_numpy(numpy_img).resize_(1, 1, h, w)
        else:
            return torch.from_numpy(numpy_img).resize_(1, 1, h, w).to(device)
    elif numpy_img.ndim == 3:
        if device is None or device == device_cpu:
            return torch.from_numpy(numpy_img.transpose(2, 0, 1)).float().unsqueeze_(0)
        else:
            return torch.from_numpy(numpy_img.transpose(2, 0, 1)).float().unsqueeze_(0).to(device)
    elif numpy_img.ndim == 4:
        if device is None or device == device_cpu:
            return torch.from_numpy(numpy_img.transpose(0, 3, 1, 2)).float()
        else:
            return torch.from_numpy(numpy_img.transpose(0, 3, 1, 2)).float().to(device)
    else:
        raise ValueError("Unsupported image type.")