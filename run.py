import time
import sys
import os
import geymolloc_utils
from geymolloc import GEymolLoc

# customizable parameters
input_data = os.path.join('data', 'skater.avi')  # video
#input_data = os.path.join('data', 'screensaver.mp4')  # video
#input_data = os.path.join('data', 'iPhone.mp4')  # video
#input_data = os.path.join('data', 'guys.jpeg')  # image
#input_data = 0  # web-cam

device = "cpu"  # PyTorch device string
max_foa_frames = 10000  # number of FOA-computation time steps

foa_processor = GEymolLoc({"w": 224,
                           "h": 224,
                           "fps": 142,
                           "y": None,
                           "alpha_c": 0.5,
                           "alpha_of": 20.0,
                           "alpha_fm": 0.0,
                           "alpha_virtual": 0.0,
                           "alpha_scale": 0.25,
                           "force_scale": 3000000.0,
                           "dissipation": 1.0,
                           "method": 'wave',
                           "c": 100,
                           "I": None,
                           "epsilon": 5,
                           "w_absorbing": 10.,  # lambda
                           "w_abs_layer": 100,
                           "w_abs_case": "constant",
                           "w_gamma": 1,
                           "w_eval": 'explicit_2',
                           "w_V": None,
                           "d_theta": 1.0,
                           "fix_threshold": 50.0,
                           "plot": False,  # turn debug-data plot on or off
                           "plot_method": 4,
                           "save_plot": False,
                           "vmin1": None,
                           "vmax1": None,
                           "vmin2": -5,
                           "vmax2": 0,
                           "sparse_saving": False
                           }, device=device)

# initializing help-variables
start_time = time.time()
h, w = foa_processor.h, foa_processor.w
external_layer = foa_processor.w_abs_layer
prev_img_cv = None
of_cv = None
of_torch = None
nn = 0

# opening the input stream and getting the first frame/image
img_cv, stream_fps, video, err_msg = geymolloc_utils.open_stream(input_data)


# preparing the first frame/image
if err_msg is not None:
    print(err_msg)
    sys.exit()
else:
    img_cv, img_torch = geymolloc_utils.prepare_img(img_cv, w, h, device)

# checking if motion will be needed (only in videos)
motion_needed = (video is not None and foa_processor.p['alpha_of'] > 0.0)

# To evaluate the optical flow comparing the first and second frames in the first foa iteration
if motion_needed:
    prev_img_cv = img_cv
    img_cv, img_torch, img_time = geymolloc_utils.get_frame_from_video(video, w, h, device)

# initializing fps-tuning-related quantities
stream_t = 0.0
stream_dt = 1.0 / stream_fps
foa_t = 0.0
foa_dt = 1.0 / foa_processor.p['fps']
updated_pair_of_img = True

print("Starting...")
print("")

# loop over frames
while nn < max_foa_frames:

    # computing optical flow
    if motion_needed and updated_pair_of_img:
        of_cv, of_torch, _ = geymolloc_utils.compute_optical_flow(prev_img_cv, img_cv, device, of_cv)

    # computing the next location of the FOA
    foa, sac, ode_time = foa_processor.time_step(img_torch, of_torch)
    foa_t += foa_dt

    # printing FOA coordinates and speed
    print("[FOA x1=%.2f,\tx2=%.2f,\tv1=%.2f,\tv2=%.2f,\tsaccade=%d]" % (foa[0] - external_layer,
                                                                        foa[1] - external_layer, foa[2], foa[3], sac))

    # showing FOA on screen (if not already plotting it with the debug tools)
    if not foa_processor.plot:
        geymolloc_utils.draw_foa(img_cv, foa, external_layer, sac)

    # counting processed frames
    nn += 1

    # moving to the next frame (if needed)
    if video is not None:
        if foa_t >= (stream_t + stream_dt):
            prev_img_cv = img_cv

            while foa_t >= (stream_t + stream_dt):
                img_cv, img_torch, img_time = geymolloc_utils.get_frame_from_video(video, w, h, device)
                stream_t += stream_dt

            updated_pair_of_img = True
        else:
            updated_pair_of_img = False

# concluding operations
elapsed_time = time.time() - start_time
if video is not None:
    video.release()

print("")
print("Elapsed: " + str(elapsed_time) + " seconds (" + str(nn / elapsed_time) + " fps)")
